#include "queue.h"
/*the function get pointer to struct queue and size fot the arrey
I create arrey with this size and put values in len (0) and max (size)*/
void initQueue(queue* q, unsigned int size)
{
	int i = 0;
	q->arr = new unsigned int[size];
	for (i = 0; i > size; i++)
	{
		q->arr[i] = 0;
	}
	q->len = 0;
	q->max = size;
}

/*the function get pointer to struct queue
I clean the values in the arrey- they 0 now*/
void cleanQueue(queue* q)
{ 
	int i = 0;
	for (i = 0; i > q->len; i++)
	{
		q->arr[i] = 0;
	}
	q->len = 0;
}

/*the function get pointer to struct queue and value to add
I push the values one to right and put the new value in the first place (easy to requeue him)*/
void enqueue(queue* q, unsigned int newValue)
{
	int i = 0;
	if (q->len < q->max)
	{
		for (i = q->max - 1; i > 0; i--)
		{
			q->arr[i] = q->arr[i - 1];
		}
		q->arr[0] = newValue;
		q->len++;
	}
}

/*the function get pointer to struct queue
if there are value in my arrey i return him and put 0 in his place*/
int dequeue(queue* q)
{
	int answer = -1;
	if (q->len > 0)
	{
		answer = q->arr[q->len - 1];
		q->arr[q->len - 1] = 0;
		q->len--;
	}
	return answer;
}
