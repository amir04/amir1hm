#include "linkedList.h"

/*get pointer to the first and value to put in the new node
return: none*/
void AddFirst(Node** head,unsigned int num)
{
	//create the node:
	Node* tmp = (Node*) malloc (sizeof(Node*));
	tmp->value = num;
	//i put the cur head as second and than move the pointer to the first
	tmp->next = (*head);
	(*head) = tmp;
}

/*get pointer to the first and remove the first node is the pointer is nut null
return: -1 or the value*/
int RemoveFirst(Node** head)
{
	int num = -1;
	//the defult is to return -1, if its not enpty i return the value of the node i delete
	if ((*head)) {
		num = (*head)->value;
		Node* tmp = (*head);
		tmp = tmp->next;
		//return to head, but now he start from the secon
		*head = tmp;
	}
	return num;
}
