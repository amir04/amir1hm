#include "utils.h"

void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* TempList = new stack();
	for (i = 0; i < size; i++)
	{
		push(TempList, nums[i]);
	}
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(TempList);
	}
	//cleanStack(TempList);
}

int* reverse10()
{
	stack* TempList = new stack();
	int* nums = new int[10];
	int i = 0, cur;
	for (i = 0; i < 10; i++)
	{
		std::cin >> cur;
		push(TempList, cur);
	}
	getchar();
	for (i = 0; i < 10; i++)
	{
		nums[i] = pop(TempList);
	}
	return nums;
}