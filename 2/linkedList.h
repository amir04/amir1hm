#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stdio.h>
#include <iostream>

/* a positive-integer value stack, and the next node */
typedef struct Node
{
	unsigned int value;
	Node* next;
} Node;

void AddFirst(Node** head,unsigned int num);
int RemoveFirst(Node** head);

#endif // LINKEDLIST_H